<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('location_id');
            $table->string('title');
            $table->text('description');
            $table->string('price');
            $table->unsignedBigInteger('status_id');
            $table->timestamps();


            $table->foreign('category_id')
            ->on('categories')
            ->references('id')
            ->onUpdate('cascade')
            ->onDelete('restrict');

            $table->foreign('location_id')
            ->on('locations')
            ->references('id')
            ->onUpdate('cascade')
            ->onDelete('restrict');

             $table->foreign('status_id')
            ->on('statuses')
            ->references('id')
            ->onUpdate('cascade')
            ->onDelete('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
