<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function category(){
    	return $this->belongsTo('\App\Category');
    }

    public function status(){
    	return $this->belongsTo('\App\Status');
    }

    public function location(){
    	return $this->belongsTo('\App\Location');
    }

    public function user(){
    	return $this->belongsTo('\App\User');
    }
}
