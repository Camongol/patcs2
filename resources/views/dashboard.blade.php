@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')

<section class="dashboard">
	@include('inc.navbar')

	<div class="container">
		<div class="create">
			<a href="post_create">Add new Post</a>
		</div>
		<hr class="line">
	</div>

	@foreach($posts as $post)
	<div class="container">
		<div class="row">

			<div class="col-lg-6">
				<img src="{{$post->image}}" alt="image">
			</div>

			<div class="col-lg-6 details">
				<h5><a href="/post_view/{{$post->id}}">Title: {{$post->title}}</a></h5>
				<p>Category : {{$post->category->name}}</p>
				<p>Location: {{$post->location->name}}</p>
				<p>Price : {{$post->price}}</p>
				<p>Details: {{$post->description}}</p>
				<small>Posted on: {{$post->created_at->format("M,d,Y")}}</small>
				<small>Last updated on: {{$post->updated_at->format("M,d,Y")}}</small>
				<div class="func">
					<div class="edit-delete">
						<a class="nav-link view-more" href="/post_view/{{$post->id}}">View Details</a>

						<a class="nav-link edit" href="/post_edit/{{$post->id}}">Edit</a>
						<form action="/post_delete/{{$post->id}}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn delete" type="submit">Delete</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<hr class="line">
		@endforeach

	</div>
</section>

@endsection
